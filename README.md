**Installation**

```
npm i git+https://gitlab.com/codebucket-solutions/spaces-upload.git
```

```
uploadToSpaces(folderName, filePath, callback, metadata)
```

**Example**

```
const uploadToSpaces = require('do-spaces')

uploadToSpaces('BAD', '/Volumes/Mayank/Downloads - SSD/EST21-220003.pdf', (data, err) => {
    
}, {"Content-Type": "image/png"})
```

**Response**

```
data: {
    ETag: '"aeffb9b1ab329a2d5851da9f746c1d0d"',
    Location: 'https://codebucket.sgp1.digitaloceanspaces.com/BAD/EST21-220003.pdf',
    key: 'BAD/EST21-220003.pdf',
    Key: 'BAD/EST21-220003.pdf',
    Bucket: 'assessts'
}
```