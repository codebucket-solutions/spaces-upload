const AWS = require("aws-sdk");
const fs = require("fs");
const path = require("path");
const { SPACES_KEY, SPACES_SECRET, BUCKET_NAME, SPACE_END_POINT } = require("./credentails");

const spacesEndpoint = new AWS.Endpoint(SPACE_END_POINT);
const s3 = new AWS.S3({
	endpoint: spacesEndpoint,
	accessKeyId: SPACES_KEY,
	secretAccessKey: SPACES_SECRET,
});

const uploadToSpaces = (
	folderName,
	filePath,
	callback,
	contentType = "image/png",
	metadata = {}
) => {
	const uploadParams = {
		Bucket: `${BUCKET_NAME}/${folderName}`,
		Body: "",
		ACL: "public-read",
		Metadata: metadata,
		ContentType: contentType,
	};

	const fileStream = fs.createReadStream(filePath);
	fileStream.on("error", err => {
		throw new Error("Error in file");
	});
	uploadParams.Body = fileStream;
	uploadParams.Key = path.basename(filePath);

	s3.upload(uploadParams, async (err, data) => {
		if (err) {
			callback({}, err);
			throw new Error("Error uploading to Spaces");
		} else callback(data, {});
	});
};

module.exports = uploadToSpaces;
